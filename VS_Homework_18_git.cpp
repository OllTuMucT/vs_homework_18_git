#include <iostream>


template <class T>

    class Stack
    {
    public:

        void Push(T NewElement)
        {
            if (IndexPastElement >= SizeofStack)
            {
                ExpansionOfStack(StepOfChangedOfStack);
            }
            PtrStack[IndexPastElement] = NewElement;
            IndexPastElement++;
        }

        void ExpansionOfStack(int StepOfChangedOfStack)
        {
            T* PtrTempStack = new T[SizeofStack + StepOfChangedOfStack];
            for (int i = 0; i < SizeofStack; i++)
            {
                PtrTempStack[i] = PtrStack[i];
            }
            delete[] PtrStack;
            PtrStack = PtrTempStack;
            SizeofStack += StepOfChangedOfStack;
        }

        T Pop()
        {
            T Element = 0;
            if (IndexPastElement > 0)
            {
                 Element = PtrStack[IndexPastElement-1];
                 std::cout << "\nDeleted element: " << Element << "\n";
                 IndexPastElement--;

                 if (IndexPastElement % StepOfChangedOfStack == 0)
                 {
                     ConstrictionOfStack(StepOfChangedOfStack);
                 }
            }
            else
                 std::cout << "\nStack is empty!\n";
            return Element;
        }

        void ConstrictionOfStack(int StepOfChangedOfStack)
        {
            SizeofStack -= StepOfChangedOfStack;
            T* PtrTempStack = new T[SizeofStack];
            for (int i = 0; i < SizeofStack; i++)
            {
                PtrTempStack[i] = PtrStack[i];
            }
            delete[] PtrStack;
            PtrStack = PtrTempStack;
        }

        void ShowSize()
        {
            std::cout << "\nSize of Stack: " << SizeofStack << "\n";
        }

        void ShowStack()
        {
           std::cout << "\n";
           for (int i = 0; i < IndexPastElement; i++)
           {
               std::cout << PtrStack[i] << " ";
           }
           std::cout << "\n";
        }

        ~Stack() 
        {
           delete[] PtrStack;
        }
    private:
        int SizeofStack = 5;
        int TempSizeofStack = 0;
        int IndexPastElement = 0;
        unsigned short int StepOfChangedOfStack = 5;
        T* PtrStack = new T[SizeofStack];
    };


    int main()
    {
        Stack <int> temp;

        for (int i = 0; i < 21; i++)
        {
            temp.Push(i);
        }

        temp.ShowStack();

        temp.ShowSize();

        temp.Pop();

        temp.ShowStack();

        temp.ShowSize();
    }